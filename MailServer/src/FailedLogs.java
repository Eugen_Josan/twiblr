import java.util.ArrayList;

/**
 * Created by eugen on 18/03/15.
 */

public class FailedLogs {

    private ArrayList listOfFailedLogs = new ArrayList();

    public synchronized void addingLog (String log){

        listOfFailedLogs.add(log);
    }

    public synchronized void removeLogs(int index){

        listOfFailedLogs.remove(index);
    }

    public synchronized boolean isEmpty(){

        return listOfFailedLogs.isEmpty();
    }

    public synchronized int size(){

        return listOfFailedLogs.size();
    }

    public synchronized String valueAt(int index){

        return (String) listOfFailedLogs.get(index);
    }
}
