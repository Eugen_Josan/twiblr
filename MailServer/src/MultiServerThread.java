import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.internet.AddressException;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by eugen on 17/03/15.
 */
public class MultiServerThread extends Thread {
    private Socket socket = null;
    private FailedLogs stack = null;

    public MultiServerThread(Socket socket, FailedLogs stack) {

        this.socket = socket;
        this.stack = stack;
    }

    @Override
    public void run() {

        String email[];
        long timeNow;
        long timer = System.currentTimeMillis();
        String messageReply, messageReply2;
        String webMessage = "";
        String error_msg = "OK";
        Boolean socket_is_opened = true;

        ServerTCPSocket process = new ServerTCPSocket();
        MessageParser parser = new MessageParser();
        SMTPsendMessage send = new SMTPsendMessage();
        ClientTCPSocketToLogger sendToLogger = new ClientTCPSocketToLogger();

        try {
            sendToLogger.open_client_TCP_socket();


            if(!stack.isEmpty()){

                for(int i = 0; i != stack.size(); i++){
                    sendToLogger.send_message(stack.valueAt(i));
                    stack.removeLogs(i);
                }
            }

        }catch (IOException e) {
            System.err.println("Logger server is down, message registration will be postponed till server is recovered");
            socket_is_opened = false;

        }

        try {

            while (!(webMessage = process.incoming_Messages_TCP_socket(socket)).equals("exit")) {

                //back up in case client connection is down, this will interrupt the thread after 60 seconds
                //in other words the maximum length of socket connection is limited to 60 seconds
                if(System.currentTimeMillis() > timer+15000) break;

                if(webMessage != "empty") {

                    System.out.println("Received a message: "+ webMessage);

                    email = parser.incoming_Text_Parser(webMessage);
                    System.out.println("The message was successfully parsed into: "+ email[0] +" | "+email[1]);

                    send.initiate_SMTP_Session();

                    try {
                        send.sendMail(email[0], email[1]);
                        System.out.println("The message was sent to " + email[0]);

                    }catch(AddressException e) {
                        error_msg = "The email address format is wrong.";
                        System.err.println(error_msg);

                    }catch (NoSuchProviderException e) {
                        error_msg = "There provider is temporarily shutdown";
                        System.err.println(error_msg);

                    }
                    catch (MessagingException e) {
                        error_msg = "The message wasn't delivered due to local smtp server failure.";
                        System.err.println(error_msg);

                    }

                    timeNow = System.currentTimeMillis();

                    messageReply = timeNow + "&mail&send&outer&" + email[2];

                    //sending back the result of message send process
                    process.outcoming_Messages_TCP_socket(socket, webMessage + " " + error_msg);


                    timeNow = System.currentTimeMillis();

                    messageReply2 = timeNow + "&mail&send&inner&" + error_msg;


                    if(socket_is_opened) {
                        try {
                            //registering the log of message sent into logging server
                            sendToLogger.send_message(messageReply);

                            //registering the log of message answer to web server into logging server
                            sendToLogger.send_message(messageReply2);

                        } catch (IOException e) {
                            error_msg = "Fail to access output stream";
                            System.err.println(error_msg);
                        }
                    }else{
                        stack.addingLog(messageReply);
                        stack.addingLog(messageReply2);
                    }
                }
            }

        } catch (IOException e) {
            System.err.println("Could not get the stream from connection on port 4444 (MailServer)");
            MultiServerThread.currentThread().stop();

        }finally{

            try {
                //end of connection
                sendToLogger.send_message("exit");
                MultiServerThread.currentThread().stop();
                socket.close();
                sendToLogger.close_client_TCP_socket();

            }catch (NullPointerException e){
               System.err.println("Warning there is no established connection with logging server, so nothing to be closed");

            }catch (IOException e){
                System.err.println("Mail: Couldn't close socket");
                MultiServerThread.currentThread().stop();
            }
        }
    }
}
