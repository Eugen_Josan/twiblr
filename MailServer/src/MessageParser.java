import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by eugen on 14/03/15.
 */
public class MessageParser {

    /**
     * Parser for extracting message ID, email address of the receiver, and the body of the email.
     *
     * @param text - message to be parsed
     * @return email array of string of size 3 with message ID, receiver email, and email body.
     */
    public String[] incoming_Text_Parser(String text) {

        //sendmail : email = "jjenea@gmail.com"&text ="some text"

        String email[] = new String[3];

        Pattern pattern = Pattern.compile("\"(.*?)\"");

        Matcher regex = pattern.matcher(text);

        regex.find();

        email[0] = text.substring(regex.start() + 1, regex.end() - 1);

        //as the grammar is fixed it will always be of the form &text=", so + 6 chars
        int start = text.indexOf("&")+7;

        email[1] = text.substring(start, text.length() - 1);


//        while(matcher.find()){
//
//            email[counter] = text.substring(matcher.start()+1, matcher.end()-1);
//            ++counter;
//        }

        email[2] = text;

        return email;
    }


//    public static void main(String[] args) {
//        MessageParser parser = new MessageParser();
//        String [] log;
//        log = parser.incoming_Text_Parser("sendmail : email = \"\"&text=\"some text\"");
//
//        //System.out.println(log);
//    }
}