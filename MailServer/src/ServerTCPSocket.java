/**
 * Created by eugen on 14/03/15.
 * @author Eugeniu Josan
 * This class is responsible for opening a TCP socket on a server side
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class ServerTCPSocket {

    private ServerSocket server = null;

    private int serverPort = 4444;

    private PrintWriter out;
    private BufferedReader in;


    /**
     * This method runs the server's socket
     * @throws IOException
     */
    public void open_TCP_Socket() throws IOException{

        // Create a Socket and bind it to a port
        server = new ServerSocket(serverPort);

        System.out.println("Server is up and running...");

        // Accept a connection from the client and associate a Socket to this connection
       // socket = server.accept();
    }
    /**
     * Getter method instance of Server Socket
     * @return server
     */
    public ServerSocket getServer() {
        return server;
    }

    /**
     * This method is listening server port for incoming messages
     * @return rcvdMessages = storing an arraylist of all incoming messages
     * @throws IOException
     */

    public String incoming_Messages_TCP_socket(Socket socket) throws IOException {

        // Create the stream of data to be communicated between this server and the client
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String line;
        // Read the request from the client
        if ((line = in.readLine()) != null) return line;
        else return "empty";

    }

    /**
     * This method is sending messages to the client through the assigned server port
     * @throws IOException
     */

    public void outcoming_Messages_TCP_socket(Socket socket, String response) throws IOException {
        if(out == null) out = new PrintWriter(socket.getOutputStream(), true);
        out.println(response);
    }

    /**
     * This method ends the TCP connection
     * @throws IOException
     */
    public void close_TCP_Socket() throws IOException {

        in.close();
        out.close();
        server.close();
    }
}
