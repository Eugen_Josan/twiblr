//
//  DatabaseHandler.cpp
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-05.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#include "DatabaseHandler.h"

DatabaseHandler* DatabaseHandler::_databaseInstance = 0;


DatabaseHandler* DatabaseHandler::Instance()
{
    if (_databaseInstance == 0)
    {
        _databaseInstance = new DatabaseHandler();
    }
    return _databaseInstance;
}

DatabaseHandler::DatabaseHandler()
{
    int errorCode = 0;
    
    errorCode = sqlite3_open("test.db", &database);
    if (errorCode != SQLITE_OK)
    { std::cerr << "Cannot opend database: " << sqlite3_errmsg(database) << std::endl;
    }
    else {std::cout << "Database opened succesfully!" <<std::endl;}
    std::vector<std::string> users = getUserList();
    std::cout << "The opened database has "<< getUserList().size() <<" registered users." <<std::endl;
}

DatabaseHandler::~DatabaseHandler()
{
    sqlite3_close(database);
}

std::vector<std::string> DatabaseHandler::gettwibles(std::string alias)
{
    char *errorMessage = 0;
    int errorCode = 0;
    
    std::string sqlRequest = "SELECT twibble FROM twibbles WHERE alias = \"" + alias + "\"";
    std::vector<std::string> answer;

    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, (void*)&answer, &errorMessage);
    
    return answer;
}



int DatabaseHandler::databaseCallback(void* data, int argc, char** argv, char** azColName)
{
    std::vector<std::string>* accumalator = (std::vector<std::string>*)data;
    for (int i = 0; i < argc; i++)
    {
        accumalator->push_back(argv[i]);
    }
    return 0;
}

bool DatabaseHandler::useradd(std::string alias, std::string email)
{
    
    time_t logtime = time(0);
    tm *loctime = localtime(&logtime);
    std::stringstream local_time;
    
    local_time << std::put_time(loctime, "%D %H:%M:%S");
    std::string sqlRequest = "INSERT INTO user_table VALUES(\""
    +alias+"\",\""
    +email+"\",\""+ local_time.str() +"\");";
    
    int errorCode = 0;
    char *errorMessage = 0;
    
    
    writer.lock();
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, 0, &errorMessage);
    writer.unlock();
    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "user already exists-" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
        throw errorReport.c_str();
    }
    sqlRequest = "INSERT INTO profiles VALUES(\""
    +alias+"\", \"\", \"\", \"\", \"\", \"\",\""+ local_time.str() +"\");";
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, 0, &errorMessage);
    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "user already exists-" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
    }

    return true;
    
}

bool DatabaseHandler::userdelete(std::string alias)
{
    std::string sqlRequest = "DELETE FROM user_table WHERE alias = \""+alias+"\";";
    int errorCode = 0;
    char *errorMessage = 0;
    
    writer.lock();
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, 0, &errorMessage);
    writer.unlock();

    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "user doesn't exists [" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
        throw errorReport.c_str();
    }
    
    return true;
}

std::vector<std::string> DatabaseHandler::userlogin(std::string alias)
{
    using namespace std::chrono;
    
    int errorCode = 0;
    char *errorMessage = 0;
    std::vector<std::string> answer;
    
    //Check if user exists
    std::string sqlRequest = "SELECT alias FROM user_table WHERE alias = \"" + alias + "\"";
    writer.lock();
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, (void*)&answer, &errorMessage);
    writer.unlock();
    if (answer.size() == 0)
    {
        throw "no such user";
    }
    
    auto timestamp = system_clock::now().time_since_epoch();
    milliseconds ms = duration_cast<milliseconds>(timestamp);
    
    sqlRequest = "INSERT INTO log_history VALUES(\""
    +alias+"\","+ std::to_string(ms.count())+");";

    writer.lock();
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, 0, &errorMessage);
    writer.unlock();

    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "[" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
        throw errorReport.c_str();
    }
    
    //TODO: enrich the answer with other usefull data
    return answer;
}

bool DatabaseHandler::subscriptionadd(std::string alias,std::string follow)
{
    int errorCode = 0;
    char *errorMessage = 0;
    std::string sqlRequest = "INSERT INTO subscription VALUES(\""
    +alias+"\",\""
    +follow+"\");";

    writer.lock();
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, 0, &errorMessage);
    writer.unlock();
    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "could not add subscription [" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
        throw errorReport.c_str();
    }
    
    return true;

}

std::vector<std::string> DatabaseHandler::getemail(std::string alias)
{
    char *errorMessage = 0;
    int errorCode = 0;
    
    std::string sqlRequest = "SELECT email FROM user_table WHERE alias = \"" + alias + "\"";
    std::vector<std::string> answer;
    
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, (void*)&answer, &errorMessage);
    
    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "[" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
        throw errorReport.c_str();
    }
    
    return answer;
}




//TODO: review this
std::vector<std::string> DatabaseHandler::getsubscription(std::string alias)
{
    char *errorMessage = 0;
    int errorCode = 0;
    std::vector<std::string> answer;
    
    std::string sqlRequest = "SELECT alias_to_watch FROM subscription WHERE alias = \"" + alias + "\"";

    
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, (void*)&answer, &errorMessage);
    
    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "[" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
        throw errorReport.c_str();
    }
    
    return answer;
}


//TODO: review this
std::vector<std::string> DatabaseHandler::getuserfollowers(std::string alias)
{
    char *errorMessage = 0;
    int errorCode = 0;
    std::vector<std::string> answer;
    std::string sqlRequest = "SELECT alias FROM subscription WHERE alias_to_watch = \"" + alias + "\"";

    
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, (void*)&answer, &errorMessage);
    
    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "[" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
        throw errorReport.c_str();
    }
    
    return answer;
}

bool DatabaseHandler::twibbleadd(std::string alias, std::string message)
{
    int errorCode = 0;
    char *errorMessage = 0;
    time_t logtime = time(0);
    tm *loctime = localtime(&logtime);
    std::stringstream local_time;
    local_time << std::put_time(loctime, "%D %H:%M:%S");
    
    std::string sqlRequest = "INSERT INTO twibbles VALUES(\""
    +alias+"\",\""
    +message+"\",\""+ local_time.str() +"\");";
    
    writer.lock();
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, 0, &errorMessage);
    writer.unlock();
    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "database error [" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
        throw errorReport.c_str();
    }
    
    return true;
}


bool DatabaseHandler::subscriptiondelete(std::string alias, std::string follow)
{
    
    std::string sqlRequest = "DELETE FROM subscription WHERE alias = \""
    +alias+"\" AND alias_to_watch = \"" + follow +"\";";
    int errorCode = 0;
    char *errorMessage = 0;
    
    writer.lock();
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, 0, &errorMessage);
    writer.unlock();
    
    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "user doesn't exists [" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
        throw errorReport.c_str();
    }
    
    return true;
}

bool DatabaseHandler::twibbledelete(std::string alias, std::string message)
{
    int errorCode = 0;
    char *errorMessage = 0;
    std::string sqlRequest = "DELETE FROM twibbles WHERE alias = \""
    +alias+"\" AND twibble LIKE \"%" + message +"%\";";
    
    writer.lock();
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, 0, &errorMessage);
    writer.unlock();
    
    if (errorCode != SQLITE_OK)
    {
        std::string  errorReport = "no such twible [" + std::string(errorMessage)+ "]";
        sqlite3_free(errorMessage);
        throw errorReport.c_str();
    }
    
    return true;
}

std::vector<std::string> DatabaseHandler::getUserList()
{
    char *errorMessage = 0;
    int errorCode = 0;
    
    std::string sqlRequest = "SELECT alias FROM user_table;";
    std::vector<std::string> answer;
    
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, (void*)&answer, &errorMessage);
    
    return answer;
}

std::vector<std::string> DatabaseHandler::executeSQL(std::string sqlRequest)
{
    char *errorMessage = 0;
    int errorCode = 0;

    std::vector<std::string> answer;
    
    errorCode = sqlite3_exec(database, sqlRequest.c_str(), databaseCallback, (void*)&answer, &errorMessage);
    
    return answer;
}
