//
//  StaticPageGenerator.cpp
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-12.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#include "StaticPageGenerator.h"

StaticPageGenerator* StaticPageGenerator::_generator = 0;


StaticPageGenerator::StaticPageGenerator()
{
    
}

StaticPageGenerator* StaticPageGenerator::Instance()
{
    if (_generator)
    {
        _generator = new StaticPageGenerator();
    }
    
    return _generator;
}

void StaticPageGenerator::generateMainPage()
{
    using namespace std;
    string index_file = "webfiles/index.html";
    std::remove(index_file.c_str());
    ofstream index(index_file.c_str(),ios::trunc);
    if (not index.is_open())
    {
        clog << "Problem opening the index file";
        return;
    }
    
    vector<string> all_users = DatabaseHandler::Instance()->getUserList();
    vector<string> twibbles;
    map<string,string> database;
    for (string user: all_users)
    {
        twibbles = DatabaseHandler::Instance()->gettwibles(user);
        for (string twibble: twibbles)
        {
            database.emplace(pair<string, string>(twibble,user));
        }
    }
    
    string header = "<!doctype html><html><head>    <meta charset=\"utf-8\">    <meta name=\"keywords\" content=\"\">    <meta name=\"description\" content=\"Project for COMP 445 Data Communication and Computer Network\">    <link rel=\"shortcut icon\" type=\"image/png\" href=\"favicon.png\" />    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">    <link rel=\"stylesheet\" type=\"text/css\" href=\"./css/bootstrap.css\">    <link rel=\"stylesheet\" id=\"ppstyle\" type=\"text/css\" href=\"style.css\">        <script src=\"./js/jquery-2.1.0.min.js\"></script>    <script src=\"./js/bootstrap.js\"></script>    <script src=\"./js/blocs.js\"></script>    <title>index - Twiblr</title></head><body><!-- Main container --><div class=\"page-container\">    <!-- Navigation Bloc --><div class=\"bloc b-parallax bgc-2 l-bloc b-divider\" id=\"nav-bloc\">	<div class=\"container bloc-sm\">		<nav class=\"navbar row\">			<div class=\"navbar-header\">				<a class=\"navbar-brand\" href=\"index.html\">Twiblr</a>				<button id=\"nav-toggle\" type=\"button\" class=\"ui-navbar-toggle navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-1\">					<span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span>				</button>			</div>			<div class=\"collapse navbar-collapse navbar-1\">				<ul class=\"site-navigation nav\">					<li>						<a href=\"index.html\" class=\"pull-left\">Home</a>					</li>					<li>						<a href=\"login.html\">Login</a>					</li>									<li>						<a href=\"allprofiles.html\">AllProfiles</a>					</li>				</ul>			</div>		</nav>	</div></div><!-- Navigation Bloc END --><!-- bloc-1 --><div class=\"bloc \" id=\"bloc-1\">	<div class=\"container bloc-lg\">		<div class=\"row\">			<div class=\"col-sm-12\">				<h2 class=\"text-center mg-sm\">					Twibles				</h2>			</div>		</div>";
    stringstream content;
    
    content << 	"<div class=\"row voffset\">";
    int counter = 0;
    for (pair<string,string> message: database)
    {
        if (counter %4 == 0 && counter != 0 )
        {
            content << "</div>" << "<div class=\"row voffset\">";
        }
        content << "<div class=\"col-sm-3\"><h3 class=\" mg-sm\">" << message.first
        << "</h3><p>" << message.second << "</p></div>";
        ++counter;
    }
    
    content << "</div>" << "</div>	</div></div><!-- bloc-1 END --></div><!-- Main container END --></body>    <!-- Google Analytics --><!-- Google Analytics END --></html>";
    
    index << header << content.str();
    
    index.close();
    
}

void StaticPageGenerator::generateProfilePage(std::string alias)
{
    using namespace std;
    std::string file_to_generate = "webfiles/" + alias + + "-twibbles.html";
    stringstream content;
    std::remove(file_to_generate.c_str());
    ofstream index(file_to_generate.c_str(),ios::trunc);
    if (not index.is_open())
    {
        clog << "Problem opening the profile file: " + file_to_generate;
        return;
    }
    
    string header = "<!doctype html> <html> <head>     <meta charset=\"utf-8\">     <meta name=\"keywords\" content=\"\">     <meta name=\"description\" content=\"Project for COMP 445 Data Communication and Computer Network\">     <link rel=\"shortcut icon\" type=\"image/png\" href=\"favicon.png\" />     <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">     <link rel=\"stylesheet\" type=\"text/css\" href=\"./css/bootstrap.css\">     <link rel=\"stylesheet\" id=\"ppstyle\" type=\"text/css\" href=\"style.css\">          <script src=\"./js/jquery-2.1.0.min.js\"></script>     <script src=\"./js/bootstrap.js\"></script>     <script src=\"./js/blocs.js\"></script>     <title>twibbles - Twiblr</title> </head> <body> <!-- Main container --> <div class=\"page-container\">      <!-- Navigation Bloc --> <div id=\"b-nav\" class=\"bloc-container\"> 	<div id=\"bloc-nav\" class=\"d-mode\"> 		<div class=\"bloc b-parallax bgc-2 l-bloc b-divider\" id=\"nav-bloc\"> 			<div class=\"container bloc-sm\"> 				<nav class=\"navbar row\"> 					<div class=\"navbar-header\"> 						<a class=\"navbar-brand\" href=\"index.html\">Twiblr</a> 						<button id=\"nav-toggle\" type=\"button\" class=\"ui-navbar-toggle navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-1\"> 							<span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span> 						</button> 					</div> 					<div class=\"collapse navbar-collapse navbar-1\"> 						<ul class=\"site-navigation nav\"> 							<li> 								<a href=\"index.html\" class=\"pull-left\">Home</a> 							</li> <!-- 							<li> 								<a href=\"login.html\">Login</a> 							</li> --> <!-- 							<li> 								<a href=\"twibbles.html\">Twibbles</a> 							</li> --> 							<li> 								<a href=\"allprofiles.html\">AllProfiles</a> 							</li> 						</ul> 					</div> 				</nav> 			</div> 		</div> 	</div> </div> <!-- Navigation Bloc END -->  <!-- bloc-4 --> <div class=\"bloc l-bloc  b-divider\" id=\"bloc-4\"> 	<div class=\"container bloc-sm\"> 		<div class=\"row\">";

    vector<string> profile = DatabaseHandler::Instance()->executeSQL("SELECT * from profiles WHERE alias = \""+alias+"\";");
    vector<string> regdate = DatabaseHandler::Instance()->executeSQL("SELECT regdate from user_table WHERE alias = \""+alias+"\";");
    
    
    content << "<div class=\"col-sm-6\"><form action=\"/\"><h2 class=\"mg-lg text-center\">My profile: "<< alias <<"</h2>";
    content << "<div class=\"form-group\"><label class=\"\"> Registered on: "<< regdate.at(0) <<"</label></div>";
    content << "<div class=\"form-group\"><label class=\"\">Name: "<< profile.at(1) <<" "<<profile.at(2) << "</label></div><div class=\"form-group\"><label class=\"\">Location: "<<profile.at(3)
    << "</label></div><div class=\"form-group\"><label class=\"\">Hobby: " <<profile.at(4) << "</label></div>"
    <<"<input type=\"hidden\" name=\"action\" value=\"editprofile\" /><input type=\"hidden\" name=\"alias\" value=\""
    << alias <<"\" /><button type=\"submit\" class=\"btn btn-clean btn-d  btn-lg pull-right\">Edit Profile</button></form> last modified: "
    << profile.at(6)<<"</div> 			<div class=\"col-sm-6\"> 				<div class=\"divider-h\"> 					<span class=\"divider\"></span> 				</div> 				<div class=\"divider-h\"> 					<span class=\"divider\"></span> 				</div> 				<h3 class=\"mg-md\"> 					About me. 				</h3> 				<p>"
    << profile.at(5)<<"</p> 			</div>";
    
    
    content << " 		</div> 	</div> </div> <!-- bloc-4 END -->  <!-- bloc-5 --> <div class=\"bloc l-bloc \" id=\"bloc-5\"> 	<div class=\"container bloc-sm\"> 		<div class=\"row\"> 			<div class=\"col-sm-12\"> 				<h3 class=\"mg-md text-center\"> 					My twibbles 				</h3> 			</div> 		</div> 	</div> </div> <!-- bloc-5 END -->  <!-- bloc-6 --> <div class=\"bloc l-bloc \" id=\"bloc-6\"> 	<div class=\"container\">";
    
    
    
    
    vector<string> twibbles = DatabaseHandler::Instance()->gettwibles(alias);
    
    content << "<div class=\"row\">";
    
    int counter = 0;
    for (string twible: twibbles)
    {
        if (counter %3 == 0 && counter != 0 )
        {
            content << "</div><br /><br /><div class=\"row\">";
        }
        content << "<form action=\"/\"><div class=\"col-sm-4\"><h3 class=\"mg-sm \">"
        << twible << "</h3><p> published on: "
        << DatabaseHandler::Instance()->executeSQL("SELECT timestamp FROM twibbles WHERE twibble LIKE \"%"+twible + "%\";").at(0)
        <<"</p><input type=\"hidden\" name=\"action\" value=\"twibbledelete\" /><input type=\"hidden\" name=\"alias\" value=\""
        << alias <<"\" /><input type=\"hidden\" name=\"message\" value=\""
        << twible << "\" /><button href=\"index.html\" type=\"submit\" class=\"btn btn-clean btn-d btn-sm btn-rd\">Delete</button></div></form>";
        ++counter;
    }


    
    
    
    
    
    
    
    
    
    
    
    content <<"</div></div></div>";
    
//    SELECT timestamp FROM twibbles WHERE twibble LIKE "%WAZ%";
    
    content <<"<div class=\"bloc l-bloc  \" id=\"bloc-7\">"
    <<"<div class=\"container bloc-md\"> 	<form action=\"/\"> <div class=\"row\"> <div class=\"col-sm-10\"> <div class=\"divider-h\"> <span class=\"divider\">"
    <<"</span></div><div class=\"form-group\"><input type=\"hidden\" name=\"action\" value=\"twibbleadd\"/><input type=\"hidden\" name=\"alias\" value=\""
    <<alias <<"\" /> 					<input class=\"form-control\" maxlength=\"100\" placeholder=\"enter your twibble here\" name=\"message\"/> 				</div> 				<div class=\"divider-h\"> 					<span class=\"divider\"></span> 				</div> 			</div> 			<div class=\"col-sm-2\"> 				<div class=\"divider-h\"> 					<span class=\"divider\"></span> 				</div> 				<div class=\"text-center\">"
    <<"<button href=\"index.html\" class=\"btn btn-d  btn-lg btn-sq btn-glossy \" type=\"submit\">Send twibble</button> 				</div> 				<div class=\"divider-h\"> 					<span class=\"divider\"></span> 				</div> 			</div> 		</div> 	</form> 	</div> </div>";
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    vector<string> subscribed = DatabaseHandler::Instance()->getsubscription(alias);
    
    content << "<div class=\"bloc l-bloc  b-divider\" id=\"bloc-8\"> 	<div class=\"container bloc-lg\"> 		<div class=\"row\"> 			<div class=\"col-sm-12\"> 				<h2 class=\"text-center mg-md\"> 					My subscriptions 				</h2> 			</div> 		</div><div class=\"row voffset\">";
    
    counter = 0;
    for (string user: subscribed)
    {
        if (counter %4 == 0)
        {
            content << "</div><div class=\"row voffset\">";
        }
        
        content <<	"<form action=\"/\"><div class=\"col-sm-3\"><h3 class=\"mg-md\">"
        << user << "<input type=\"hidden\" name=\"action\" value=\"unsubscribe\" /><input type=\"hidden\" name=\"alias\" value=\""
        << alias <<"\" /><input type=\"hidden\" name=\"follow\" value=\"" <<user <<"\" /></h3><button href=\"login.html\" type=\"submit\" class=\"btn btn-clean btn-d btn-sm btn-rd\">Unsibscribe</button></div></form>";
        ++counter;
    }

    
    
    
    
    
    content << "</div> 	</div> </div> <!-- bloc-8 END --> </div> <!-- Main container END -->  </body>      <!-- Google Analytics -->  <!-- Google Analytics END -->  </html>";
    
    index << header << content.str();
    
    index.close();
    
}


void StaticPageGenerator::generateAllProfilesPage()
{
    using namespace std;
    string index_file = "webfiles/allprofiles.html";
    std::remove(index_file.c_str());
    ofstream index(index_file.c_str(),ios::trunc);
    if (not index.is_open())
    {
        clog << "Problem opening the index file";
        return;
    }
    
    vector<string> all_users = DatabaseHandler::Instance()->getUserList();
    map<string,string> database;
    string registration_date;
    string querry;
    for (string user: all_users)
    {
        querry = "SELECT regdate FROM user_table WHERE alias = \"" + user + "\";";
        registration_date = DatabaseHandler::Instance()->executeSQL(querry).at(0);
        database.emplace(pair<string, string>(user,registration_date));
    }
    
    string header = "<!doctype html><html><head>    <meta charset=\"utf-8\">    <meta name=\"keywords\" content=\"\">    <meta name=\"description\" content=\"Project for COMP 445 Data Communication and Computer Network\">    <link rel=\"shortcut icon\" type=\"image/png\" href=\"favicon.png\" />    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">    <link rel=\"stylesheet\" type=\"text/css\" href=\"./css/bootstrap.css\">    <link rel=\"stylesheet\" id=\"ppstyle\" type=\"text/css\" href=\"style.css\">        <script src=\"./js/jquery-2.1.0.min.js\"></script>    <script src=\"./js/bootstrap.js\"></script>    <script src=\"./js/blocs.js\"></script>    <title>allprofiles - Twiblr</title></head><body><!-- Main container --><div class=\"page-container\">    <!-- Navigation Bloc --><div id=\"b-nav\" class=\"bloc-container\">	<div id=\"bloc-nav\" class=\"d-mode\">		<div class=\"bloc b-parallax bgc-2 l-bloc b-divider\" id=\"nav-bloc\">			<div class=\"container bloc-sm\">				<nav class=\"navbar row\">					<div class=\"navbar-header\">						<a class=\"navbar-brand\" href=\"index.html\">Twiblr</a>						<button id=\"nav-toggle\" type=\"button\" class=\"ui-navbar-toggle navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-1\">							<span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span>						</button>					</div>					<div class=\"collapse navbar-collapse navbar-1\">						<ul class=\"site-navigation nav\">							<li>								<a href=\"index.html\" class=\"pull-left\">Home</a>							</li>							<li>								<a href=\"login.html\">Login</a>							</li>									<li>								<a href=\"allprofiles.html\">AllProfiles</a>							</li>						</ul>					</div>				</nav>			</div>		</div>	</div></div><!-- Navigation Bloc END --><!-- bloc-9 --><div class=\"bloc l-bloc \" id=\"bloc-9\">	<div class=\"container bloc-lg\">";
    stringstream content;
    
    content << 	"<div class=\"row\">";
    int counter = 0;
    for (pair<string,string> message: database)
    {
        if (counter %3 == 0 && counter != 0 )
        {
            content << "</div>" << "<div class=\"row\">";
        }
        content << "<div class=\"col-sm-4\"><h3 class=\" mg-md\">" << message.first
        << "</h3><h5 class=\"mg-md\">Registererd on: " << message.second << "</h5><p> About: NOT YET IMPLEMENTED</p></div>";
        ++counter;
    }
    
    content << "</div>" << "</div></div><!-- bloc-1 END --></div><!-- Main container END --></body>    <!-- Google Analytics --><!-- Google Analytics END --></html>";
    
    index << header << content.str();
    
    index.close();
    
}








