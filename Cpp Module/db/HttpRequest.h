//
//  HttpRequest.h
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-11.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#ifndef Twiblr_Request_h
#define Twiblr_Request_h

struct HttpRequest
{
    std::string instruction;
    std::string path_file;
    std::string file_extension;
    std::map<std::string, std::string> paramters;
    float http_version;
};

#endif
