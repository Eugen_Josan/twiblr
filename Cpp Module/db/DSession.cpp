//
//  DSession.cpp
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-06.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#include "DSession.h"

namespace basio = boost::asio;

#define OK "OK\n"

DSession::DSession(boost::asio::ip::tcp::socket socket)
:clientSocket(std::move(socket))
{
    std::clog << "Serving user request from " << clientSocket.remote_endpoint().address() << std::endl;
    startServing();
//    this->~DSession();
}


DSession::~DSession()
{
    std::clog << "Connection terminated by user."<<std::endl;
    clientSocket.close();
}




void DSession::startServing()
{
    namespace time = std::chrono;
    const size_t buffer_size = 1024;
    char buff[buffer_size] = {0};
    size_t mess_len;
    std::string client_request;
    time::milliseconds timestamp;
    Operation type;
    

    while (true)
    {
        
        try
        {
            memset(buff, 0, buffer_size);
            mess_len = clientSocket.read_some(basio::buffer(buff));
            client_request = std::string(buff,mess_len);
            timestamp = time::duration_cast<time::milliseconds>(time::system_clock::now().time_since_epoch());
            
            if (client_request.length() == 2) {break;}
            
            logIt("&web&send&inner&"+client_request);
            type = getOperationType(client_request);
            
            switch (type)
            {
                case Operation::useradd:
                    useradd(client_request);
                    break;
                case Operation::userdelete:
                    userdelete(client_request);
                    break;
                case Operation::subscriptionadd:
                    subscriptionadd(client_request);
                    break;
                case Operation::subscriptiondelete:
                    subscriptiondelete(client_request);
                    break;
                case Operation::twibbleadd:
                    twibbleadd(client_request);
                    break;
                case Operation::twibbledelete:
                    twibbledelete(client_request);
                    break;
                case Operation::twiblemodify:
                    twibblemodify(client_request);
                    break;
                case Operation::userlogin:
                    userlogin(client_request);
                    break;
                case Operation::getemail:
                    getemail(client_request);
                    break;
                case Operation::getsubscription:
                    getsubscription(client_request);
                    break;
                case Operation::gettwibles:
                    gettwibles(client_request);
                    break;
                case Operation::getall:
                    getall(client_request);
                    break;
                case Operation::getuserfollowers:
                    getuserfollowers(client_request);
                    break;
                case Operation::getusers:
                    getusers();
                    break;
                case Operation::modifyprofile:
                    modifyProfile(client_request);
                    break;
                default:
                    throw "unknown request type";
            }
            
        } catch (const char* error_description)
        {
            std::stringstream fail_message;
            fail_message <<"FAIL: " << error_description<<"\n";
            answerAndLogIt(fail_message.str());
        } catch (std::exception &exp)
        {
            std::clog << "Client connection closing ..." << std::endl;
            clientSocket.close();
            return;
        }
        
    }
   logIt("&web&send&inner&Connection closed by client request\n");
}


Operation DSession::getOperationType(const std::string clientRequest)
{
    tokens = request_parser.getTokens(clientRequest);

    if (tokens.at("instruction").compare("useradd") == 0) {return Operation::useradd;}
    if (tokens.at("instruction").compare("userdelete") == 0) { return Operation::userdelete;}
    if (tokens.at("instruction").compare("subscriptionadd") == 0) { return Operation::subscriptionadd;}
    if (tokens.at("instruction").compare("subscriptiondelete") == 0) { return Operation::subscriptiondelete;}
    if (tokens.at("instruction").compare("twibbleadd") == 0) { return Operation::twibbleadd;}
    if (tokens.at("instruction").compare("twibbledelete") == 0) { return Operation::twibbledelete;}
    if (tokens.at("instruction").compare("twiblemodify") == 0) { return Operation::twiblemodify;}
    if (tokens.at("instruction").compare("userlogin") == 0) { return Operation::userlogin;}
    if (tokens.at("instruction").compare("getemail") == 0) { return Operation::getemail;}
    if (tokens.at("instruction").compare("getsubscription") == 0) { return Operation::getsubscription;}
    if (tokens.at("instruction").compare("gettwibles") == 0) { return Operation::gettwibles;}
    if (tokens.at("instruction").compare("getall") == 0) { return Operation::getall;}
    if (tokens.at("instruction").compare("getuserfollowers") == 0) { return Operation::getuserfollowers;}
    if (tokens.at("instruction").compare("getusers") == 0) { return Operation::getusers;}
    if (tokens.at("instruction").compare("modifyprofile") == 0) { return Operation::modifyprofile;}
    
    return Operation::unknown;
}

void DSession::useradd(std::string details)
{
    //TODO:: Implement verification of email format
    
    std::string alias = tokens.at("alias");
    if (tokens.find("email") == tokens.end()) { throw "missing email";}
    std::string email = tokens.at("email");

    DatabaseHandler::Instance()->useradd(alias, email);
    answerAndLogIt(OK);
    
    //TODO: executIt async
    sendMail(email, "Hi, you've been registered to Twibble.\n Welcome to the club.\n Administrator");
    StaticPageGenerator::Instance()->generateProfilePage(alias);
    StaticPageGenerator::Instance()->generateAllProfilesPage();
}

void DSession::userdelete(std::string details)
{
    std::string alias = tokens.at("alias");
    DatabaseHandler::Instance()->userlogin(alias);
    DatabaseHandler::Instance()->userdelete(alias);
    
    std::vector<std::string> followers = DatabaseHandler::Instance()->getuserfollowers(alias);
    
    for (std::string follower: followers)
    {
        DatabaseHandler::Instance()->subscriptiondelete(follower, alias);
    }
    
    std::string querry = "DELETE FROM profiles WHERE alias = \"" + alias +"\";";
    DatabaseHandler::Instance()->executeSQL(querry);
    
    StaticPageGenerator::Instance()->generateAllProfilesPage();
    answerAndLogIt(OK);
}

void DSession::subscriptionadd(std::string details)
{
    std::string alias = tokens.at("alias");
    std::string follow = tokens.at("follow");
    DatabaseHandler::Instance()->subscriptionadd(alias, follow);
    answerAndLogIt(OK);
    StaticPageGenerator::Instance()->generateProfilePage(alias);
}

void DSession::subscriptiondelete(std::string details)
{
    std::string alias = tokens.at("alias");
    std::string follow = tokens.at("follow");
    DatabaseHandler::Instance()->subscriptiondelete(alias, follow);
    answerAndLogIt(OK);
    StaticPageGenerator::Instance()->generateProfilePage(alias);
}

void DSession::twibbleadd(std::string details)
{
    std::string alias = tokens.at("alias");
    std::string twible = tokens.at("message");
    DatabaseHandler::Instance()->twibbleadd(alias, twible);
    answerAndLogIt(OK);
    sendmailToAllSubscribers(alias, twible);
    StaticPageGenerator::Instance()->generateMainPage();
    StaticPageGenerator::Instance()->generateProfilePage(alias);
}

void DSession::twibbledelete(std::string details)
{
    std::string alias = tokens.at("alias");
    std::string twible = tokens.at("message");
    DatabaseHandler::Instance()->twibbledelete(alias, twible);
    answerAndLogIt(OK);
    StaticPageGenerator::Instance()->generateProfilePage(alias);
    StaticPageGenerator::Instance()->generateMainPage();
}

void DSession::twibblemodify(std::string details)
{
    //TODO:: Implement twiblemodify
    throw "TWIBLEMODIFY NOT YET IMPLEMENTED";
}

void DSession::userlogin(std::string details)
{
    std::string alias = tokens.at("alias");
    DatabaseHandler::Instance()->userlogin(alias);
    answerAndLogIt(OK);
}

void DSession::getemail(std::string details)
{
    std::string alias = tokens.at("alias");
    DatabaseHandler::Instance()->userlogin(alias);
    std::vector<std::string> result = DatabaseHandler::Instance()->getemail(alias);
    answerAndLogIt(result.at(0)+"\n");

}

void DSession::getsubscription(std::string details)
{
    answer = DatabaseHandler::Instance()->getsubscription(tokens.at("alias"));
    if (answer.size() == 0)
    {
        throw "this user is subscribed to nobody";
    }
    
    std::string message = "";
    
    for(std::string subscription: answer)
    {
        message += subscription + "&";
    }
    message.at(message.size()-1) = '\n';
    answerAndLogIt(message);
}

void DSession::gettwibles(std::string details)
{
    answer = DatabaseHandler::Instance()->gettwibles(tokens.at("alias"));
    if (answer.size() == 0)
    {
        throw "no twibles of this user";
    }
    
    std::string message = "";
    
    for(std::string twible: answer)
    {
        message += twible + "&";
    }
    message.at(message.size()-1) = '\n';
    answerAndLogIt(message);
}

void DSession::getall(std::string details)
{
    //TODO:: Implement getall
    throw "GETALL NOT YET IMPLEMENTED";
}



void DSession::getuserfollowers(std::string details)
{
    std::string alias = tokens.at("alias");
    DatabaseHandler::Instance()->userlogin(alias);
    std::vector<std::string> answer = DatabaseHandler::Instance()->getuserfollowers(alias);
    std::string message = "";
    
    for(std::string subscription: answer)
    {
        message += subscription + "&";
    }
    message.at(message.size()-1) = '\n';
    answerAndLogIt(message);
    
}

void DSession::getusers()
{
    answer = DatabaseHandler::Instance()->getUserList();
    if (answer.size() == 0)
    {
        throw "there are no registered users";
    }
    
    std::string message = "";
    
    for(std::string user: answer)
    {
        message += user + "&";
    }
    message.at(message.size()-1) = '\n';
    answerAndLogIt(message);
}


#pragma mark LOGGING
void DSession::answerAndLogIt(std::string message)
{
    logIt("&db&send&inner&"+message);
    clientSocket.write_some(basio::buffer(message));
}


void DSession::logIt(std::string message)
{
    using namespace std::chrono;
    boost::system::error_code socket_error;
    local_log_access.lock();
    basio::ip::tcp::socket socket(clientSocket.get_io_service());
    local_log_access.unlock();
    milliseconds timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
    std::string output = std::to_string(timestamp.count()) + message;
    socket.connect(basio::ip::tcp::endpoint(basio::ip::address::from_string("104.236.36.201"),9999), socket_error);
    if (socket_error != boost::system::errc::errc_t::success)
    {
        std::cerr << "Could not connect: "<< socket_error.message()<< "[saving local to database.log]"<< std::endl;
        saveLocalLog(output);
//        std::cerr.flush();
        return;
    }
    pushLocalLog(socket);
    socket.write_some(basio::buffer(output));
    std::clog << output <<std::endl;
    socket.close();
}

void DSession::saveLocalLog(std::string message)
{
    local_log_access.lock();
    std::fstream local_log("database.log",std::ios::app);
    if (local_log.is_open())
    {
        local_log << message;
        local_log.close();
    }
    else
    {
        std::cerr << "Could not access database.log to write" << std::endl;
    }

    local_log_access.unlock();
}


void DSession::pushLocalLog(boost::asio::ip::tcp::socket& socket)
{
    std::string line;
    local_log_access.lock();
    std::fstream local_log("database.log",std::ios::in);
    if (local_log.is_open())
    {
        while (getline(local_log, line))
        {
            socket.write_some(basio::buffer(line + "\n"));
        }
        local_log.close();
        std::ofstream cleaner("database.log",std::ios::trunc);
        
        if (cleaner.is_open()) {cleaner.close();} //erase file
    }
    else
    {
        std::cerr << "Could not access database.log to read" << std::endl;
    }
    
    local_log_access.unlock();
}


#pragma mark MAIL

void DSession::sendMail(std::string email, std::string message)
{
    using namespace std;
    string output = "sendmail:email=\"" + email + "\"&text=\"" + message + "\"";

    boost::system::error_code socket_error;
    basio::ip::tcp::socket socket(clientSocket.get_io_service());
    
    socket.connect(basio::ip::tcp::endpoint(basio::ip::address::from_string("104.236.36.201"),4446), socket_error);
    if (socket_error != boost::system::errc::errc_t::success)
    {
        std::cerr << "Could not connect: "<< socket_error.message()<< "[retry later]"<< std::endl;

        return;
    }
    clog << output << std::endl;
    
    logIt("&db&send&inner&"+output);
    socket.write_some(basio::buffer(output));
    socket.close();

    
}


void DSession::modifyProfile(std::string details)
{
    using namespace std;
    string alias = tokens.at("alias");
    string first = tokens.at("first");
    string second = tokens.at("second");
    string location = tokens.at("location");
    string hobby = tokens.at("hobby");
    string about = tokens.at("about");

    time_t logtime = time(0);
    tm *loctime = localtime(&logtime);
    std::stringstream local_time;
    local_time << std::put_time(loctime, "%D %H:%M:%S");
    
    std::string querry = "UPDATE profiles SET firstname = \"" + first
    + "\", secondname =  \"" + second
    + "\", location =  \"" + location
    + "\", hobby =  \"" + hobby
    + "\", about =  \"" + about
    + "\", moddate =  \"" + local_time.str()
    + "\" WHERE alias = \"" + alias + "\";";
    
    DatabaseHandler::Instance()->executeSQL(querry.c_str());
    answerAndLogIt(OK);
    
    //TODO: executIt async
    StaticPageGenerator::Instance()->generateProfilePage(alias);
}


void DSession::sendmailToAllSubscribers(std::string alias, std::string msg)
{
    std::vector<std::string> subscribers = DatabaseHandler::Instance()->getuserfollowers(alias);
    std::string message = "Dear user, \nSince you are subscribed to "
    + alias + "'s messages,\nWe would like to inform you that he just posted a message \nwith the following content: " + msg;
    std::string email;
    
    for (std::string follower : subscribers)
    {
        email = DatabaseHandler::Instance()->getemail(follower).at(0);
        std::thread(&DSession::sendMail,this, email,message).detach();
    }
}
