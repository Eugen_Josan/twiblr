//
//  main.cpp
//  db
//
//  Created by Denis Grigor on 2015-04-04.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#include <iostream>
#include <thread>
#include "DSession.h"


using namespace std;
namespace basio = boost::asio;

void serveRequest(basio::ip::tcp::socket socket)
{
    DSession client(std::move(socket));
}

int main(int argc, const char * argv[])
{
    basio::io_service service;
    try
    {
        basio::ip::tcp::acceptor acceptor(service,basio::ip::tcp::endpoint(basio::ip::tcp::v4(), 7777));

        DatabaseHandler::Instance();
//        StaticPageGenerator::Instance();
        StaticPageGenerator::Instance()->generateMainPage();
        StaticPageGenerator::Instance()->generateAllProfilesPage();
        cout << "Serving database on port 7777" << endl;

        while (true)
        {
            basio::ip::tcp::socket clientSocket(service);
            acceptor.accept(clientSocket);
            thread(serveRequest, std::move(clientSocket)).detach();
        }
    }
    catch(std::exception & exp)
    {
        cerr << "Could not start server due to " << exp.what() <<endl;
        exit(EXIT_FAILURE);
    }
    

    return 0;
}
