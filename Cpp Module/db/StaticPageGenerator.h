//
//  StaticPageGenerator.h
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-12.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#ifndef __Twiblr__StaticPageGenerator__
#define __Twiblr__StaticPageGenerator__

#include <stdio.h>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <cstdio>
#include "DatabaseHandler.h"

class StaticPageGenerator
{
public:
    static StaticPageGenerator* Instance();
    void generateMainPage();
    void generateProfilePage(std::string alias);
    void generateAllProfilesPage();
private:
    StaticPageGenerator();
    std::string file_path;
    static StaticPageGenerator* _generator;
};

#endif /* defined(__Twiblr__StaticPageGenerator__) */
