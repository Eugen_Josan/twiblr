//
//  RequestParser.cpp
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-05.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#include "RequestParser.h"

std::map<std::string, std::string> RequestParser::getTokens(std::string msg)
{
    using namespace std;
    string message = (msg.at(msg.length()-1) == '\n') ? msg : msg + "\n";
    map<string,string> answer;
    string token_header;
    string token_content = extractInstruction(message);
    answer.emplace(pair<string,string>("instruction",token_content));
    while (message.length() != 0)
    {
        token_header = extractParameter(message);
        token_content = extractContent(message);
        answer.emplace(pair<string, string>(token_header,token_content));
    }
    
    return answer;
}

std::string RequestParser::extractInstruction(std::string &message)
{
    std::string answer;
    for(int cursor = 0; cursor != message.length(); ++cursor)
    {
        if(message.at(cursor) == ':')
        {
            message = message.substr(cursor+1, message.length());
            return answer;
        }
        answer.push_back(message.at(cursor));
    }
   throw "bad instruction format";
}




std::string RequestParser::extractParameter(std::string &message)
{
    std::string answer;
    for(int cursor = 0; cursor != message.length(); ++cursor)
    {
        if(message.at(cursor) == '=')
        {
            message = message.substr(cursor+1, message.length());
            return answer;
        }
        if (std::isalnum(message.at(cursor))) { answer.push_back(message.at(cursor));}
        else {throw "invalid parameter format";}
    }
    throw "bad instruction format";
}


std::string RequestParser::extractContent(std::string &message)
{
    std::string answer;
    if (message.length() == 0) { throw "content empty";}
    if (message.at(0) != '\"') { throw "bad content format";}
    message = message.substr(1,message.length()); //remove first content quote
    for(int cursor = 0; cursor != message.length(); ++cursor)
    {
        if(message.at(cursor) == '&'
           || message.at(cursor) == '\r'
           || message.at(cursor) == '\n')
        {
            message = (message.at(cursor) != '\r') ? message.substr(cursor+1, message.length()): "";
            if (answer.at(answer.length()-1) == '\"') {answer.pop_back();}
            return answer;
        }
        if (std::isprint(message.at(cursor))) {answer.push_back(message.at(cursor));}
    }
    throw "bad instruction format";
}

HttpRequest RequestParser::parseHttpRequest(std::string msg)
{
    HttpRequest answer;
    std::vector<std::string> components;
    std::string element;
    std::string message = msg;
    
    for (char letter: message)
    {
        if (letter == '\n' || letter == '\r'){break;}
        if (letter == ' ' || letter == '?')
        {
            components.push_back(element);
            element = "";
        }
        else {element.push_back(letter);}
    }

    answer.instruction = components.at(0);
    answer.path_file = components.at(1).substr(1);
    answer.file_extension = parseFileExtension(answer.path_file);
    answer.paramters = parseParameters(components);

    answer.http_version = 1.1;

    return answer;
}


std::string RequestParser::parseFileExtension(std::string path)
{
    auto reverse = [](std::string toReverse)
    {
        std::string temp;
        for (uint i = (uint)(toReverse.size()-1); i != -1; --i)
        {
            temp.push_back(toReverse.at(i));
        }
        return temp;
    };
    
    std::string temp = reverse(path);
    std::string answer;
    int position = (int)temp.find('.');
    if (position != std::string::npos)
    {
        answer = reverse(temp.substr(0, position));
    }
    
    return answer;
}


std::map<std::string,std::string> RequestParser::parseParameters(std::vector<std::string> message)
{
    #define KEY 0
    #define VALUE 1
    using namespace std;
    map<string,string> answer;
    
    if (message.size() < 3) { return answer;}
    
    vector<string> pairs;
    string buffer;
    bool position = 0;

    for (auto character: message.at(2))
    {
        if (character == 'H') {break;}
        if (character == '=' && position == KEY)
        {
            position = VALUE;
            pairs.push_back(buffer);
            buffer = "";
        }
        else if (character == '&' || character == ' ')
        {
            position = KEY;
            pairs.push_back(buffer);
            buffer = "";
            cleanHttpMarkers(pairs.at(VALUE));
            answer.emplace(pair<string,string>(pairs.at(KEY), pairs.at(VALUE)));
            pairs.clear();
        }
        else
        {
            buffer.push_back(character);
        }
    }
    cleanHttpMarkers(buffer);
    answer.emplace(pair<string,string>(pairs.at(KEY),buffer));
    
    return answer;
}


void RequestParser::cleanHttpMarkers(std::string& message)
{
    int position;
    while ((position = (int)message.find("%40")) != std::string::npos)
    {
        message.replace(position, 3, "@");
        position += 1;
    }
    while ((position = (int)message.find("%2B")) != std::string::npos)
    {
        message.replace(position, 3, " ");
        position += 1;
    }
    while ((position = (int)message.find("%3f")) != std::string::npos)
    {
        message.replace(position, 3, "?");
        position += 1;
    }
    
    while ((position = (int)message.find("%09")) != std::string::npos)
    {
        message.replace(position, 3, "\t");
        position += 1;
    }
    while ((position = (int)message.find("%2C")) != std::string::npos)
    {
        message.replace(position, 3, ",");
        position += 1;
    }
    while ((position = (int)message.find("+")) != std::string::npos)
    {
        message.replace(position, 1, " ");
        position += 1;
    }
    

}
