//
//  DatabaseHandler.h
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-05.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#ifndef __Twiblr__DatabaseHandler__
#define __Twiblr__DatabaseHandler__

#include <iostream>
#include <sqlite3.h>
#include <string>
#include <vector>
#include <mutex>
#include <chrono>
#include <iomanip>
#include <sstream>

class DatabaseHandler
{
public:
    static DatabaseHandler* Instance();
    bool useradd(std::string alias, std::string email);         //DONE
    bool userdelete(std::string alias);                         //DONE
    bool subscriptionadd(std::string alias,std::string follow); //DONE
    bool subscriptiondelete(std::string alias, std::string follow);//DONE
    bool twibbleadd(std::string alias, std::string message);    //DONE
    bool twibbledelete(std::string alias, std::string message); //DONE
    bool twibblemodify(std::string alias, std::string oldmessage, std::string newmessage);
    std::vector<std::string> userlogin(std::string alias);      //DONE
    std::vector<std::string> getemail(std::string alias);       //DONE
    std::vector<std::string> getsubscription(std::string alias);//DONE
    std::vector<std::string> gettwibles(std::string alias);     //DONE
    std::vector<std::string> getall(std::string alias);
    std::vector<std::string> getuserfollowers(std::string alias);//DONE
    std::vector<std::string> getUserList();
    std::vector<std::string> executeSQL(std::string querry);
private:
    DatabaseHandler();
    ~DatabaseHandler();
    static DatabaseHandler* _databaseInstance;
    sqlite3* database;
    std::mutex writer;
    

    
    /**
     Callback function used to process the sql querry returnrs
     @param data expected to be of type vector<string>
     @returns 0
     */
    static int databaseCallback(void* data, int argc, char** argv, char** azColName);
    
};

#endif /* defined(__Twiblr__DatabaseHandler__) */
