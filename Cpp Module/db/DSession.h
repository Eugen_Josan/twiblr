//
//  DSession.h
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-06.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#ifndef __Twiblr__DSession__
#define __Twiblr__DSession__

#include <stdio.h>
#include <string>
#include <fstream>
#include <mutex>
#include <thread>
#include <exception>
#include "RequestParser.h"
#include "StaticPageGenerator.h"

#include <boost/asio.hpp>

enum class Operation: uint
{
    unknown,
    useradd,
    userdelete,
    subscriptionadd,
    subscriptiondelete,
    twibbleadd,
    twibbledelete,
    twiblemodify,

    userlogin,
    getemail,
    getsubscription,
    getuserfollowers,
    gettwibles,
    getall,
    getusers,
    modifyprofile
};

class DSession
{
public:
    DSession(boost::asio::ip::tcp::socket clientSocket);
    ~DSession();
private:
    std::mutex local_log_access;
    std::mutex page_generator;
    boost::asio::ip::tcp::socket clientSocket;
    RequestParser request_parser;
    std::map<std::string, std::string> tokens;
    std::vector<std::string> answer;
    void startServing();
    void logIt(std::string message);
    void answerAndLogIt(std::string answer);
    Operation getOperationType(const std::string clientRequest);
    
    void useradd(std::string details);
    void userdelete(std::string details);
    void subscriptionadd(std::string details);
    void subscriptiondelete(std::string details);
    void twibbleadd(std::string details);
    void twibbledelete(std::string details);
    void twibblemodify(std::string details);
    void userlogin(std::string details);
    void getemail(std::string details);
    void getsubscription(std::string details);
    void getuserfollowers(std::string details);
    void gettwibles(std::string details);
    void getall(std::string details);
    void getusers();
    void modifyProfile(std::string details);

    //AUXILIARY
    void saveLocalLog(std::string message);
    void pushLocalLog(boost::asio::ip::tcp::socket& socket);
    void sendMail(std::string , std::string);
    void sendmailToAllSubscribers(std::string alias, std::string message);
    
};

#endif /* defined(__Twiblr__DSession__) */
