//
//  RequestParser.h
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-05.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#ifndef __Twiblr__RequestParser__
#define __Twiblr__RequestParser__

#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <cctype>
#include <cstring>
#include "HttpRequest.h"


class RequestParser
{
public:
    RequestParser() = default;
    ~RequestParser()= default;
    
    bool validateRequest(std::string request);
    std::map<std::string, std::string> getTokens(std::string message);
    HttpRequest parseHttpRequest(std::string message);
    
    
    //TODO: Make it private at cleaning
    std::string extractInstruction(std::string &message);
    std::string extractParameter(std::string &message);
    std::string extractContent(std::string &message);
    std::string parseFileExtension(std::string path);
    std::map<std::string,std::string> parseParameters(std::vector<std::string>);
private:
    void cleanHttpMarkers(std::string&);
};

#endif /* defined(__Twiblr__RequestParser__) */
