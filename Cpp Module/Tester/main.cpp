//
//  main.cpp
//  Tester
//
//  Created by Denis Grigor on 2015-04-05.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#include <iostream>
#include <boost/asio.hpp>
#include <thread>
#include <string>

#define THREAD_NUMBER 10

using namespace boost::asio::ip;


void clientConnection(tcp::resolver::iterator endpoint, boost::asio::io_service & service)
{
    try
    {
        tcp::socket clientSocket(service);
        boost::asio::connect(clientSocket, endpoint);
        std::stringstream tempStreem;
        tempStreem << std::this_thread::get_id();
        int delay = 0;
        std::string loginName = tempStreem.str();
        boost::system::error_code err;
        
        boost::asio::write(clientSocket,boost::asio::buffer(loginName),err);
        for (int i = 0; i != 10; ++i)
        {
            std::stringstream temp;
            std::string message;
            delay = arc4random_uniform(10000);
            std::this_thread::sleep_for(std::chrono::milliseconds(delay));
            temp << "I've been waiting for " << delay << " ms." << std::endl;
            message = temp.str();
            boost::asio::write(clientSocket,boost::asio::buffer(message));
        }
        
        boost::asio::write(clientSocket,boost::asio::buffer("I am tired and I go home."));
        clientSocket.close();
    } catch (std::exception &exp)
    {
        std::cerr << "Error in thread " << std::this_thread::get_id() << ": " << exp.what() << std::endl;
        return;
    }
    
}



int main(int argc, const char * argv[])
{
    boost::asio::io_service service;
    
    tcp::resolver resolver(service);
    tcp::resolver::query queryAddress("localhost","9999");
    tcp::resolver::iterator endpointIterator = resolver.resolve(queryAddress);
    std::vector<std::thread> threads;
    
    for (int i = 0; i != THREAD_NUMBER; ++i)
    {
//        tcp::socket client_socket(service);
//        boost::asio::connect(client_socket, endpointIterator);
        threads.push_back(std::thread(clientConnection, std::ref(endpointIterator), std::ref(service)));
    }
    
    for (int i = 0; i != THREAD_NUMBER; ++i)
    {
        threads.at(i).join();
    }
    
    
    return 0;
}
