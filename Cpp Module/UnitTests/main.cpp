//
//  main.cpp
//  UnitTests
//
//  Created by Denis Grigor on 2015-03-23.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#include "gtest/gtest.h"
#include "RequestParser.h"

RequestParser parser;

TEST (ExtractInstruction, ExtractValidInstruction)
{
    std::string request = "see:";

    EXPECT_EQ(parser.extractInstruction(request), "see");
    EXPECT_EQ(request, "");
    
    request = "intruct:this";
    EXPECT_EQ(parser.extractInstruction(request), "intruct");
    EXPECT_EQ(request, "this");
}

TEST (ExtractInstruction, ExtractNonValidInstruction)
{
    std::string request = "";
    
    EXPECT_ANY_THROW(parser.extractInstruction(request));
    try {
        parser.extractInstruction(request);
    } catch (const char* exception)
    {
        EXPECT_EQ(exception, "bad instruction format");
    }

    request = "instruction";
    EXPECT_ANY_THROW(parser.extractInstruction(request));
    try {
        parser.extractInstruction(request);
    } catch (const char* exception)
    {
        EXPECT_EQ(exception, "bad instruction format");
    }
}


TEST (ParsingRequest, ParseValidRequest)
{
    std::string request = "useradd:alias=\"denix\"&email=\"s@s\"\r\n";
    
    std::map<std::string, std::string> answer = parser.getTokens(request);
    
    EXPECT_EQ(answer.at("instruction"), "useradd");
    EXPECT_EQ(answer.at("alias"), "denix");
    EXPECT_EQ(answer.at("email"), "s@s");
    EXPECT_EQ(answer.size(), 3);
    
}


TEST(ParsingRequest, HttpRequest)
{
    std::string request = "GET /pic_mountain.jpg HTTP/1.1";
    HttpRequest answer = parser.parseHttpRequest(request);
    
    EXPECT_EQ(answer.instruction, "GET");
    EXPECT_EQ(answer.path_file, "pic_mountain.jpg");
//    EXPECT_EQ(answer.http_version, 1.1);
    
    request = "GET /pic_mountain.jpg HTTP/1.1\n\r";
    answer = parser.parseHttpRequest(request);
    EXPECT_EQ(answer.instruction, "GET");
    EXPECT_EQ(answer.path_file, "pic_mountain.jpg");
    //    EXPECT_EQ(answer.http_version, 1.1);
}


TEST(ParsingRequest, parameterHttpRequest)
{
    std::string request = "GET /pic_mountain.jpg HTTP/1.1";
    HttpRequest answer = parser.parseHttpRequest(request);
    
    EXPECT_EQ(answer.instruction, "GET");
    EXPECT_EQ(answer.path_file, "pic_mountain.jpg");
    EXPECT_EQ(answer.paramters.size(), 0);
    
    request = "GET /?alias=denix HTTP/1.1\n\r";
    answer = parser.parseHttpRequest(request);
    EXPECT_EQ(answer.instruction, "GET");
    EXPECT_EQ(answer.path_file, "");
    EXPECT_EQ(answer.paramters.size(), 1);
    ASSERT_NO_THROW(answer.paramters.at("alias"));
    EXPECT_EQ(answer.paramters.at("alias"), "denix");
    //    EXPECT_EQ(answer.http_version, 1.1);
}

TEST(ParsingRequest, multiparameterHttpRequest)
{

    std::string request = "GET /?alias=denis&email=see%40mee.here HTTP/1.1\n\r";
    HttpRequest answer = parser.parseHttpRequest(request);
    EXPECT_EQ(answer.instruction, "GET");
    EXPECT_EQ(answer.path_file, "");
    EXPECT_EQ(answer.paramters.size(), 2);
    ASSERT_NO_THROW(answer.paramters.at("alias"));
    EXPECT_EQ(answer.paramters.at("alias"), "denis");
    
    ASSERT_NO_THROW(answer.paramters.at("email"));
    EXPECT_EQ(answer.paramters.at("email"), "see@mee.here");
}

TEST(ParsingRequest, filextensionHttpRequest)
{
    std::string request = "GET /pic_mountain.jpg HTTP/1.1";
    HttpRequest answer = parser.parseHttpRequest(request);
    
    EXPECT_EQ(answer.instruction, "GET");
    EXPECT_EQ(answer.path_file, "pic_mountain.jpg");
    EXPECT_EQ(answer.file_extension, "jpg");

    
    request = "GET /?alias=denis&email=see%40mee.here HTTP/1.1\n\r";
    answer = parser.parseHttpRequest(request);
    EXPECT_EQ(answer.instruction, "GET");
    EXPECT_EQ(answer.path_file, "");
    EXPECT_EQ(answer.file_extension, "");

    
    request = "GET /js/jquery-2.1.0.min.js HTTP/1.1\n\r";
    answer = parser.parseHttpRequest(request);
    EXPECT_EQ(answer.instruction, "GET");
    EXPECT_EQ(answer.path_file, "js/jquery-2.1.0.min.js");
    EXPECT_EQ(answer.file_extension, "js");
    
}

TEST(ParsingRequest, asciiCharParseHttpRequest)
{
    std::string request = "GET /?action=twibbledelete&alias=arnold&message=de%2Bcorectat%2Bencodincul HTTP/1.1";
    HttpRequest answer = parser.parseHttpRequest(request);
    
    EXPECT_EQ(answer.instruction, "GET");
    EXPECT_EQ(answer.path_file, "");
    EXPECT_EQ(answer.paramters.size(), 3);
    ASSERT_NO_THROW(answer.paramters.at("message"));
    EXPECT_EQ(answer.paramters.at("message"), "de corectat encodincul");
    //    EXPECT_EQ(answer.http_version, 1.1);
    
}










int main(int argc, char * argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}