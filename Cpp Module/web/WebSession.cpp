//
//  WebSession.cpp
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-15.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#include "WebSession.h"

namespace basio = boost::asio;

const std::string WebSession::header = "HTTP/1.1 200 OK\r\n\r\n";


WebSession::WebSession(boost::asio::ip::tcp::socket socket)
:_client_socket(std::move(socket))
{
    std::clog << "Serving user request from " << _client_socket.remote_endpoint().address() << std::endl;
    startServing();
}

void WebSession::startServing()
{
    using namespace std;
    
    const size_t buffer_size = 1024;
    char buf[buffer_size] = {0};
    size_t message_size;
    string received_message;
    stringstream answer;
    RequestType client_type_request;
    
    try {
        memset(buf, 0, buffer_size);
        message_size = _client_socket.read_some(basio::buffer(buf));
        received_message = string(buf, message_size);
        request = _request_parser.parseHttpRequest(received_message);
        if (request.path_file == ""){ request.path_file = "index.html";}
        
        client_type_request = identifyRequestType(request);
        
        
        //output filter
        if (client_type_request != RequestType::resources_request)
        {
            stdout_mutex.lock();
            clog << "client requested: " << request.path_file << " with parameters: ";
            for (auto parameter: request.paramters)
            {
                clog << "(" <<parameter.first << " = " << parameter.second << "); ";
            }
            clog << endl;
            stdout_mutex.unlock();
        }
        
        switch (client_type_request)
        {
            case RequestType::registration:
                performRegistration(request);
                redirectToPage("login.html");
                break;
                
            case RequestType::login:
                performLoging(request);
                redirectToPage(request.paramters.at("alias")
                                   + "-twibbles.html");
                break;
            case RequestType::resources_request:
                redirectToPage(request.path_file);
                break;
                
            case RequestType::page_request:
                redirectToPage(request.path_file);
                break;
                
            case RequestType::twibble_add:
                performAddTwibble(request);
                redirectToPage("index.html");
                break;
                
            case RequestType::twibble_delete:
                performDeleteTwibble(request);
                redirectToPage(request.paramters.at("alias")
                               + "-twibbles.html");
                break;
                
            case RequestType::unsubscription:
                performUnsubscription(request);
                redirectToPage(request.paramters.at("alias")
                               + "-twibbles.html");
                break;
                
            case RequestType::profile_edit:
                performProfileEdit(request.paramters.at("alias"));
                break;
                
            case RequestType::updateprofile:
                performProfileUpdate(request);
                redirectToPage("index.html");
                break;
            case RequestType::userdelete:
                performUserDelete(request.paramters.at("alias"));
                redirectToPage("index.html");
                break;

            default:
                break;
        }

    }
    catch (const char* exp)
    {
        //TODO: implement redirecting to error page
        cerr << "There was an error: " << exp << endl;
    }
    
    catch (std::exception &exp)
    {
        clog << "Timeout ... closing connection ..." << endl;
    }
    _client_socket.close();
}




RequestType WebSession::identifyRequestType(HttpRequest client_request)
{
    if (client_request.file_extension == "js"
        || client_request.file_extension == "css")
    {return RequestType::resources_request;}
    
    if (client_request.paramters.size() == 0)
    {return RequestType::page_request;}
    
    if (client_request.paramters.find("action") != client_request.paramters.end()
        && client_request.paramters.at("action") == "useradd")
    {return RequestType::registration;}
    
    if (client_request.paramters.find("action") != client_request.paramters.end()
        && client_request.paramters.at("action") == "login")
    {return RequestType::login;}
    
    if (client_request.paramters.find("action") != client_request.paramters.end()
        && client_request.paramters.at("action") == "twibbleadd")
    {return RequestType::twibble_add;}
    
    if (client_request.paramters.find("action") != client_request.paramters.end()
        && client_request.paramters.at("action") == "twibbledelete")
    {return RequestType::twibble_delete;}
    
    if (client_request.paramters.find("action") != client_request.paramters.end()
        && client_request.paramters.at("action") == "subscribe")
    {return RequestType::subscription;}
    
    if (client_request.paramters.find("action") != client_request.paramters.end()
        && client_request.paramters.at("action") == "unsubscribe")
    {return RequestType::unsubscription;}
    if (client_request.paramters.find("action") != client_request.paramters.end()
        && client_request.paramters.at("action") == "editprofile")
    {return RequestType::profile_edit;}
    if (client_request.paramters.find("action") != client_request.paramters.end()
        && client_request.paramters.at("action") == "updateprofile")
    {return RequestType::updateprofile;}
    if (client_request.paramters.find("action") != client_request.paramters.end()
        && client_request.paramters.at("action") == "userdelete")
    {return RequestType::userdelete;}
    
    
    
    
    
    return RequestType::page_request;
}

void WebSession::redirectToPage(std::string page)
{
    std::stringstream fetched_page;
    std::string pagefile = "webfiles/" + page;
    int counter = 3;
    while (counter != 0)
    {
        fetched_page = std::stringstream();
        std::fstream file(pagefile.c_str());
        if (file)
        {
            std::string line;
            while (getline(file, line)){fetched_page << line;}
            file.close();
            break;
        }
        else
        {
            //TODO: Implement redirection to error page
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            fetched_page << " Server busy!!!" << std::endl;
            --counter;
        }
    }
    
    _client_socket.write_some(basio::buffer(header + fetched_page.str()));
}


void WebSession::logIt(std::string message)
{
    using namespace std::chrono;
    boost::system::error_code socket_error;
    basio::ip::tcp::socket socket(_client_socket.get_io_service());
    milliseconds timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
    std::string output = std::to_string(timestamp.count()) + message;
    socket.connect(basio::ip::tcp::endpoint(basio::ip::address::from_string("104.236.36.201"),9999), socket_error);
    if (socket_error != boost::system::errc::errc_t::success)
    {
        std::cerr << "Could not connect: "<< socket_error.message()<< "[saving local to database.log]"<< std::endl;
//        saveLocalLog(output + "&local");
        //        std::cerr.flush();
        return;
    }
//    pushLocalLog(socket);
    socket.write_some(basio::buffer(output + "&realtime"));
    std::clog << output <<std::endl;
    socket.close();
}


void WebSession::performRegistration(HttpRequest client_request)
{
    logIt("&web&recv&outer&Registration request: alias = " +
          client_request.paramters.at("alias") + " email = " +
          client_request.paramters.at("email"));
    std::clog << "Performing registration ...." << std::endl;
    boost::system::error_code socket_error;
    basio::ip::tcp::socket socket(_client_socket.get_io_service());

    std::string output = "useradd:alias=\"" + client_request.paramters.at("alias")+"\"&email=\"" + client_request.paramters.at("email") + "\"";
    socket.connect(basio::ip::tcp::endpoint(basio::ip::address::from_string("127.0.0.1"),7777), socket_error);
    if (socket_error != boost::system::errc::errc_t::success)
    {
        std::cerr << "Could not connect: "<< socket_error.message()<< "[saving local to web.log]"<< std::endl;
        //        saveLocalLog(output + "&local");
        //        std::cerr.flush();
        return;
    }
    //    pushLocalLog(socket);
    socket.write_some(basio::buffer(output));
    std::clog << output <<std::endl;
    
    char buf[1024] = {0};
    size_t answer_lenght = socket.read_some(basio::buffer(buf));
    std::string answer = std::string(buf,answer_lenght);
    socket.close();
    if (answer.at(0) != 'O')
    {
        throw answer.c_str();
    }
}

void WebSession::performLoging(HttpRequest client_request)
{
    logIt("&web&recv&outer&Login: alias = " +
          client_request.paramters.at("alias"));
    std::clog << "Logging ...." << std::endl;
    boost::system::error_code socket_error;
    basio::ip::tcp::socket socket(_client_socket.get_io_service());
    
    std::string output = "userlogin:alias=\"" + client_request.paramters.at("alias");
    socket.connect(basio::ip::tcp::endpoint(basio::ip::address::from_string("127.0.0.1"),7777), socket_error);
    if (socket_error != boost::system::errc::errc_t::success)
    {
        std::cerr << "Could not connect: "<< socket_error.message()<< "[saving local to web.log]"<< std::endl;
        //        saveLocalLog(output + "&local");
        //        std::cerr.flush();
        return;
    }
    //    pushLocalLog(socket);
    socket.write_some(basio::buffer(output));
    std::clog << output <<std::endl;
    
    char buf[1024] = {0};
    size_t answer_lenght = socket.read_some(basio::buffer(buf));
    std::string answer = std::string(buf,answer_lenght);
    socket.close();
    if (answer.at(0) != 'O')
    {
        throw answer.c_str();
    }
}


void WebSession::performAddTwibble(HttpRequest client_request)
{
    logIt("&web&recv&outer&Adding Twibble: alias = " +
          client_request.paramters.at("alias") + " message = " +
          client_request.paramters.at("message"));
    std::clog << "Adding twibble ...." << std::endl;
    boost::system::error_code socket_error;
    basio::ip::tcp::socket socket(_client_socket.get_io_service());
    
    std::string output = "twibbleadd:alias=\"" + client_request.paramters.at("alias")+"\"&message=\"" + client_request.paramters.at("message") + "\"";
    socket.connect(basio::ip::tcp::endpoint(basio::ip::address::from_string("127.0.0.1"),7777), socket_error);
    if (socket_error != boost::system::errc::errc_t::success)
    {
        std::cerr << "Could not connect: "<< socket_error.message()<< "[saving local to web.log]"<< std::endl;
        //        saveLocalLog(output + "&local");
        //        std::cerr.flush();
        return;
    }
    //    pushLocalLog(socket);
    socket.write_some(basio::buffer(output));
    std::clog << output <<std::endl;
    
    char buf[1024] = {0};
    size_t answer_lenght = socket.read_some(basio::buffer(buf));
    std::string answer = std::string(buf,answer_lenght);
    socket.close();
    if (answer.at(0) != 'O')
    {
        throw answer.c_str();
    }
}


void WebSession::performDeleteTwibble(HttpRequest client_request)
{
    logIt("&web&recv&outer&Deleting Twibble: alias = " +
          client_request.paramters.at("alias") + " message = " +
          client_request.paramters.at("message"));
    std::clog << "Deleting twibble ...." << std::endl;
    boost::system::error_code socket_error;
    basio::ip::tcp::socket socket(_client_socket.get_io_service());
    
    std::string output = "twibbledelete:alias=\"" + client_request.paramters.at("alias")+"\"&message=\"" + client_request.paramters.at("message") + "\"";
    socket.connect(basio::ip::tcp::endpoint(basio::ip::address::from_string("127.0.0.1"),7777), socket_error);
    if (socket_error != boost::system::errc::errc_t::success)
    {
        std::cerr << "Could not connect: "<< socket_error.message()<< "[saving local to web.log]"<< std::endl;
        //        saveLocalLog(output + "&local");
        //        std::cerr.flush();
        return;
    }
    //    pushLocalLog(socket);
    socket.write_some(basio::buffer(output));
    std::clog << output <<std::endl;
    
    char buf[1024] = {0};
    size_t answer_lenght = socket.read_some(basio::buffer(buf));
    std::string answer = std::string(buf,answer_lenght);
    socket.close();
    if (answer.at(0) != 'O')
    {
        throw answer.c_str();
    }
}





void WebSession::performUnsubscription(HttpRequest client_request)
{
    logIt("&web&recv&outer&unsubscribing: alias = "
          + client_request.paramters.at("alias")
          + " from  user "
          + client_request.paramters.at("follow"));
    std::clog << "Unsubscribing ...." << std::endl;
    boost::system::error_code socket_error;
    basio::ip::tcp::socket socket(_client_socket.get_io_service());
    
    std::string output = "subscriptiondelete:alias=\"" + client_request.paramters.at("alias")+"\"&follow=\"" + client_request.paramters.at("follow") + "\"";
    socket.connect(basio::ip::tcp::endpoint(basio::ip::address::from_string("127.0.0.1"),7777), socket_error);
    if (socket_error != boost::system::errc::errc_t::success)
    {
        std::cerr << "Could not connect: "<< socket_error.message()<< "[saving local to web.log]"<< std::endl;
        //        saveLocalLog(output + "&local");
        //        std::cerr.flush();
        return;
    }
    //    pushLocalLog(socket);
    socket.write_some(basio::buffer(output));
    std::clog << output <<std::endl;
    
    char buf[1024] = {0};
    size_t answer_lenght = socket.read_some(basio::buffer(buf));
    std::string answer = std::string(buf,answer_lenght);
    socket.close();
    if (answer.at(0) != 'O')
    {
        throw answer.c_str();
    }
}

void WebSession::performProfileEdit(std::string alias)
{
    std::stringstream content;
    content << header;
    content << "<!doctype html><html><head>    <meta charset=\"utf-8\">    <meta name=\"keywords\" content=\"\">    <meta name=\"description\" content=\"Project for COMP 445 Data Communication and Computer Network\">    <link rel=\"shortcut icon\" type=\"image/png\" href=\"favicon.png\" />    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">    <link rel=\"stylesheet\" type=\"text/css\" href=\"./css/bootstrap.css\">    <link rel=\"stylesheet\" id=\"ppstyle\" type=\"text/css\" href=\"style.css\">        <script src=\"./js/jquery-2.1.0.min.js\"></script>    <script src=\"./js/bootstrap.js\"></script>    <script src=\"./js/blocs.js\"></script>    <title>editprofile - Twiblr</title></head><body><!-- Main container --><div class=\"page-container\">    <!-- Navigation Bloc --><div id=\"b-nav\" class=\"bloc-container\">	<div id=\"bloc-nav\" class=\"d-mode\">		<div class=\"bloc b-parallax bgc-2 l-bloc b-divider\" id=\"nav-bloc\">			<div class=\"container bloc-sm\">				<nav class=\"navbar row\">					<div class=\"navbar-header\">						<a class=\"navbar-brand\" href=\"index.html\">Twiblr</a>						<button id=\"nav-toggle\" type=\"button\" class=\"ui-navbar-toggle navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-1\">							<span class=\"sr-only\">Toggle navigation</span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span><span class=\"icon-bar\"></span>						</button>					</div>					<div class=\"collapse navbar-collapse navbar-1\">						<ul class=\"site-navigation nav\">							<li>								<a href=\"index.html\" class=\"pull-left\">Home</a>							</li>							<li>								<a href=\"allprofiles.html\">AllProfiles</a>							</li>						</ul>					</div>				</nav>			</div>		</div>	</div></div><!-- Navigation Bloc END --><!-- bloc-10 --><div class=\"bloc l-bloc \" id=\"bloc-10\">	<div class=\"container bloc-lg\">		<div class=\"row\">			<div class=\"col-sm-4\">				<span class=\"empty-column\"></span>			</div>			<form action=\"/\">			<div class=\"col-sm-4\">				<h4 class=\"mg-md text-center\">					Profile Editor				</h4>				<div class=\"form-group\">					<label>						First Name					</label>					<input class=\"form-control\" name=\"first\"/>				</div>				<div class=\"form-group\">					<label>						Second Name					</label>					<input class=\"form-control\" name=\"second\"/>				</div>				<div class=\"form-group\">					<label>						Location					</label>					<input class=\"form-control\" name=\"location\"/>				</div>				<div class=\"form-group\">					<label>						Main Hobby					</label>					<input class=\"form-control\" name=\"hobby\"/>				</div>				<div class=\"form-group\">					<label>						About yourself					</label>					<textarea class=\"form-control\" rows=\"4\" cols=\"50\" name=\"about\">					</textarea>				<input type=\"hidden\" name=\"action\" value=\"updateprofile\" /><input type=\"hidden\" name=\"alias\" value=\""
    << alias
    <<"\" />				</div><button type=\"submit\" href=\"index.html\" class=\"btn btn-clean btn-d  btn-lg pull-right\">Save Profile</button>			</div>			</form>			<form action=\"/\">			<input type=\"hidden\" name=\"action\" value=\"userdelete\" />			<input type=\"hidden\" name=\"alias\" value=\""
    << alias
    <<"\" />			<button type=\"submit\" href=\"index.html\" class=\"btn btn-clean btn-d  btn-lg pull-right\">DELETE Profile</button>			</form>			<div class=\"col-sm-4\">				<span class=\"empty-column\"></span>			</div>		</div>	</div></div><!-- bloc-10 END --></div><!-- Main container END --></body>    <!-- Google Analytics --><!-- Google Analytics END --></html>";
    
    _client_socket.write_some(basio::buffer(content.str()));
}


void WebSession::performProfileUpdate(HttpRequest client_request)
{
    logIt("&web&recv&outer&Updateing profile for: alias = " +
          client_request.paramters.at("alias"));
    std::clog << "Updateing profile ...." << std::endl;
    boost::system::error_code socket_error;
    basio::ip::tcp::socket socket(_client_socket.get_io_service());
          
    std::string output = "modifyprofile:alias=\""
          + client_request.paramters.at("alias")
          + "\"&first=\""
          + client_request.paramters.at("first")
          + "\"&second=\""
          + client_request.paramters.at("second")
          + "\"&location=\""
          + client_request.paramters.at("location")
          + "\"&hobby=\""
          + client_request.paramters.at("hobby")
          + "\"&about=\""
          + client_request.paramters.at("about")
          + "\"";
          
    socket.connect(basio::ip::tcp::endpoint(basio::ip::address::from_string("127.0.0.1"),7777), socket_error);
    if (socket_error != boost::system::errc::errc_t::success)
    {
        std::cerr << "Could not connect: "<< socket_error.message()<< "[saving local to web.log]"<< std::endl;
        //        saveLocalLog(output + "&local");
        //        std::cerr.flush();
        return;
    }
    //    pushLocalLog(socket);
    socket.write_some(basio::buffer(output));
    std::clog << output <<std::endl;
    
    char buf[1024] = {0};
    size_t answer_lenght = socket.read_some(basio::buffer(buf));
    std::string answer = std::string(buf,answer_lenght);
    socket.close();
    if (answer.at(0) != 'O')
    {
        throw answer.c_str();
    }

}


void WebSession::performUserDelete(std::string alias)
{
    logIt("&web&recv&outer&delte profile: alias = "
          + alias);
    std::clog << "Deleting profile ...." << std::endl;
    std::string pagefile = "webfiles/" + alias+ "-twibbles.html";
    std::remove(pagefile.c_str());
    boost::system::error_code socket_error;
    basio::ip::tcp::socket socket(_client_socket.get_io_service());
    
    std::string output = "userdelete:alias=\""
    + alias
    + "\"";
    socket.connect(basio::ip::tcp::endpoint(basio::ip::address::from_string("127.0.0.1"),7777), socket_error);
    if (socket_error != boost::system::errc::errc_t::success)
    {
        std::cerr << "Could not connect: "<< socket_error.message()<< "[saving local to web.log]"<< std::endl;
        //        saveLocalLog(output + "&local");
        //        std::cerr.flush();
        return;
    }
    //    pushLocalLog(socket);
    socket.write_some(basio::buffer(output));
    std::clog << output <<std::endl;
    
    char buf[1024] = {0};
    size_t answer_lenght = socket.read_some(basio::buffer(buf));
    std::string answer = std::string(buf,answer_lenght);
    socket.close();
    if (answer.at(0) != 'O')
    {
        throw answer.c_str();
    }
}






