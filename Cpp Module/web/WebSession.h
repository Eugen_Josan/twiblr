//
//  WebSession.h
//  Twiblr
//
//  Created by Denis Grigor on 2015-04-15.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#ifndef __Twiblr__WebSession__
#define __Twiblr__WebSession__

#include <stdio.h>
#include <boost/asio.hpp>
#include <fstream>
#include <mutex>
#include <utility>
#include <thread>
#include <chrono>
#include "../db/RequestParser.h"


enum class RequestType: uint
{
    resources_request,
    page_request,
    login,
    registration,
    twibble_add,
    twibble_delete,
    subscription,
    unsubscription,
    profile_edit,
    updateprofile,
    userdelete
};

class WebSession
{
public:
    WebSession(boost::asio::ip::tcp::socket client_socket);
    ~WebSession() = default;
private:
    std::mutex stdout_mutex;
    void startServing();
    boost::asio::ip::tcp::socket _client_socket;
    RequestParser _request_parser;
    HttpRequest request;
    static const std::string header;
    void logIt(std::string);
    
    
    RequestType identifyRequestType(HttpRequest);
    void performRegistration(HttpRequest);
    void performLoging(HttpRequest);
    void redirectToPage(std::string);
    void performAddTwibble(HttpRequest);
    void performDeleteTwibble(HttpRequest);
    void performUnsubscription(HttpRequest);
    void performProfileEdit(std::string);
    void performProfileUpdate(HttpRequest);
    void performUserDelete(std::string alias);

};

#endif /* defined(__Twiblr__WebSession__) */
