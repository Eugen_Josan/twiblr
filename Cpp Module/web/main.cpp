//
//  main.cpp
//  web
//
//  Created by Denis Grigor on 2015-04-04.
//  Copyright (c) 2015 Denis Grigor. All rights reserved.
//

#include <iostream>
#include <thread>

#include "WebSession.h"

namespace basio = boost::asio;
int counter = 0;
std::mutex outputLock;

void request_handler(basio::ip::tcp::socket clientSocket)
{
    WebSession client(std::move(clientSocket));
}


int main(int argc, const char * argv[])
{

    int port = 8080;
    basio::io_service service;
    try
    {
        basio::ip::tcp::acceptor acceptor(service, basio::ip::tcp::endpoint(basio::ip::tcp::v4(),port));

        std::cout << "Starting Webserver on port " << port << std::endl;
        
        while (true)
        {
            basio::ip::tcp::socket client_socket(service);
            acceptor.accept(client_socket);
//            std::thread(request_handler,std::move(client_socket)).detach();
            request_handler(std::move(client_socket));
        }
    }
    catch(std::exception & exp)
    {
        std::cerr << "Could not start server due to " << exp.what() <<std::endl;
        exit(EXIT_FAILURE);
    }
    return 0;
}
