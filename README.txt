Denis Grigor 6708811
Eugeniu Josan 6632882

The final project consists of 4 modules (Db, Web, Mail, Logging) the first 2 are done in C++ the last 2 in java. In C++ we used SQLight for storing all the data related to users/subscribers. In Java we used MongoDB (noSQL) for storing/registering the logs. 

Our final implementation implies the use of Docker containers each module is runned in a separate container, the communication between containers is done through TCP connection. The MongoDB also need to be runned on separate container. 
