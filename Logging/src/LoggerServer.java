/**
 * Created by eugen on 14/03/15.
 * @author Eugeniu Josan
 * This class is responsible for opening a TCP socket on a server side
 */

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;



public class LoggerServer {

    private ServerSocket server = null;

    private int serverPort = 9999;

    private BufferedReader in;


    /**
     * This method runs the server's socket
     * @throws IOException
     */
    public void open_TCP_Socket() throws IOException{

        // Create a Socket and bind it to a port
        server = new ServerSocket(serverPort);

        System.out.println("Server is up and running...");

        // Accept a connection from the client and associate a Socket to this connection
        // socket = server.accept();
    }
    /**
     * Getter method instance of Server Socket
     * @return server
     */
    public ServerSocket getServer() {
        return server;
    }

    /**
     * This method is listening server port for incoming messages
     * @return rcvdMessages = storing an arraylist of all incoming messages
     * @throws IOException
     */

    public String incoming_Messages_TCP_socket(Socket socket) throws IOException {

        // Create the stream of data to be communicated between this server and the client
        if(in == null) in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String line;

        // Read the request from the client
        line = in.readLine();
        if ((line != null) || (line != "")) return line;
        else return "empty";

    }

    /**
     * This method ends the TCP connection
     * @throws IOException
     */
    public void close_TCP_Socket() throws IOException {

        in.close();
        server.close();
    }
}
