/**
 * Created by eugen on 14/03/15.
 * @author Eugeniu Josan
 * the main class for running the Web Server
 */

import java.io.IOException;


public class MainLogger {
    public static void main(String[] args) {

        LoggerServer process = new LoggerServer();

        try {
            process.open_TCP_Socket();
        } catch (IOException e) {
            System.out.println("Logger: Could not open socket");
            System.exit(-1);
        }

        boolean listening = true;

        while (listening) {
            MultiServerThread thread_work;
            try {
                thread_work = new MultiServerThread(process.getServer().accept());
                Thread t = new Thread(thread_work);
                t.start();
            }catch (IOException e) {
                System.out.println("Logger: Accept failed: 9999");
                System.exit(-1);
            }
        }
        System.err.println("Logger: Could not listen on port 9999");
        System.exit(-1);

    }
}
