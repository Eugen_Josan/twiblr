/**
 * Created by eugen on 05/04/15.
 */

import com.mongodb.*;


import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseMongo {

    private String server_address = "mongo";
    //private String server_address = "localhost";
    private String database_name = "test";
    private String table_name = "table";
    private MongoClient mongoClient;
    private DB database;
    private DBCollection table;
    private BasicDBObject doc;
    Logger mongoLogger = Logger.getLogger("org.mongodb.driver");




    public DatabaseMongo(){
        mongoClient = new MongoClient(server_address);
        database = mongoClient.getDB(database_name);
        table = database.getCollection(table_name);
        mongoLogger.setLevel(Level.WARNING);
    }

    public void storeLogs(Map<String, String> log){

        doc = new BasicDBObject();

        for (Map.Entry<String, String> entry : log.entrySet()) {
            String key = entry.getKey().toString();
            String value = entry.getValue().toString();

            doc.append(key, value);
        }

        table.insert(doc);
    }

    //method used for testing
    public void findLogByEventID(int event_id){
        BasicDBObject query = new BasicDBObject("EventID", event_id);
        DBCursor cursor = table.find(query);
        try{
            while (cursor.hasNext()){
                System.out.println(cursor.next());
            }
        }finally {
            cursor.close();
        }
    }

    //method for testing
    public void printAll(){
        DBCursor cursor = table.find();
        try{
            while (cursor.hasNext()){
                System.out.println(cursor.next());
            }
        }finally {
            cursor.close();
        }
    }

//        for (String s : mongoClient.getDatabaseNames()) {
//            System.out.println(s);
//        }
//
//
//        List<DBObject> list = coll.getIndexInfo();
//
//        for (DBObject o : list) {
//            System.out.println(o.get("key"));
//        }


}
