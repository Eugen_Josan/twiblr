
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by eugen on 14/03/15.
 */
public class MessageParser {

    private static enum Fields {
        TIME, SERVICEID, ACTION, LOCALITY,
        RECORD
    }
    /**
     *  Parser for extracting message ID, email address of the receiver, and the body of the email.
     *  @param text - message to be parsed
     *  @return email array of string of size 3 with message ID, receiver email, and email body.
     */
    public Map<String, String> incoming_Text_Parser(String text) {

        Map<String, String> log_fields = new LinkedHashMap();
        int counter = 0;

//      timeNow&ServiceID&Action&Locality&Record

        if(check_the_log(text)) {


            // Pattern pattern = Pattern.compile("\"(.*?)\"");

            //Matcher matcher = pattern.matcher(text);

            int start = 0, end;

            while (counter != 4) {

                end = text.indexOf("&", start);
                log_fields.put(Fields.values()[counter].toString(), text.substring(start, end));

                start = end + 1;
                counter++;

                if (counter == 4)
                    log_fields.put(Fields.values()[counter].toString(), text.substring(start, text.length()));
            }


//        while (matcher.find()) {
//                log_fields.put(Fields.values()[counter].toString(), text.substring(matcher.start() + 1, matcher.end() - 1));
//                ++counter;
//                System.out.println(text.substring(matcher.start() + 1, matcher.end() - 1));
//
//            }


          return log_fields;

        }else return null;
    }

    private boolean check_the_log(String log){

        String log_uppercase = log.toUpperCase();

        String tokens = "";

        for(Fields i : Fields.values()){

            if (!i.equals(Fields.TIME)) tokens = tokens + "|";
            tokens = tokens +"&"+ i.toString();
        }

        Pattern pattern = Pattern.compile("&");
        Matcher matcher = pattern.matcher(log);

        int c= 0;

        while(matcher.find()) ++c;

        return (c >=(Fields.values().length - 1));
    }


//    public static void main(String[] args) {
//        MessageParser parser = new MessageParser();
//        Map log;
//        log = parser.incoming_Text_Parser("timeNow&ServiceID&Action&Locality&Record");
//
//        //System.out.println(log);
//    }
}