import java.io.IOException;
import java.net.Socket;
import java.util.Map;

/**
 * Created by eugen on 17/03/15.
 */
public class MultiServerThread extends Thread {
    private Socket socket = null;

    public MultiServerThread(Socket socket) { this.socket = socket; }

   @Override
    public void run() {

       Map<String, String> data_to_store;
       String webLog = "";

       LoggerServer process = new LoggerServer();
       MessageParser parser = new MessageParser();
       DatabaseMongo database = new DatabaseMongo();

       try {
           while (!(webLog = process.incoming_Messages_TCP_socket(socket)).equals("exit")) {

               if (webLog != "empty") {
                   System.out.println("Received message: "+ webLog);
                   data_to_store = parser.incoming_Text_Parser(webLog);

                   if (data_to_store != null) database.storeLogs(data_to_store);
                   else break;
               }else break;
           }

       } catch (IOException e) {
           System.err.println("Logger: Could not get the input stream from connection on port 9999 (Logging Server)");
           MultiServerThread.currentThread().stop();

       }finally {
           try {
               socket.close();
           } catch (IOException e) {
               System.err.println("Logger: Couldn't close the socket");
           }
           MultiServerThread.currentThread().stop();
       }
   }
}
