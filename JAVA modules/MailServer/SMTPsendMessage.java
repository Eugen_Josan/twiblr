/**
 * Created by eugen on 14/03/15.
 * @author Eugeniu Josan
 * This class is responsible for sending a message to a receiver's email
 */

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SMTPsendMessage {

    //private String host = "localhost";
    //private int port = 25;
    private String host = "smtp.gmail.com";
    private int port = 587;
    private String username = "hello.twiblr@gmail.com";
    private String testString  = "kjasghfd2kga1qaz2wsx3q2w1ee3rt5ys3a4k4h".substring(12, 21);
    private Session session;

    /**
     *  Will initiate smtp protocol for message sending
     */
    public void initiate_SMTP_Session(){

        Properties properties = System.getProperties();
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true");
        //properties.put("mail.smtp.ssl.trust", "localhost");

        session = Session.getInstance(properties);
    }


    /**
     *  Sending message by email through smtp
     *  @param to - receiver email
     *  @param message - body of the sending
     *  @throws MessagingException
     *  @throws AddressException
     *  @throws NoSuchProviderException
     */

    public void sendMail(String to, String message) throws MessagingException {

        MimeMessage outgoing_message = new MimeMessage(session);
        outgoing_message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        outgoing_message.setSubject("Twiblr");
        outgoing_message.setText(message);

        Transport transport = session.getTransport("smtp");
        transport.connect(host, port, username, testString);
        transport.sendMessage(outgoing_message, outgoing_message.getAllRecipients());
        transport.close();

    }
}
