/**
 * Created by eugen on 14/03/15.
 * @author Eugeniu Josan
 * this class is instantiating a client side connection using TCP with a logger
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;


public class ClientTCPSocketToLogger {

    private Socket socket = null;

    private PrintWriter out;
    private BufferedReader in;
    private BufferedReader userInput;
    private String netAddress = "logging";
   // private String netAddress = "localhost";
    private int serverPort = 9999;

    /**
     * This method opens the clients socket for TCP connection
     * @throws IOException
     */
    public void open_client_TCP_socket() throws IOException {

        InetAddress host = InetAddress.getByName(netAddress);
        socket = new Socket(host, serverPort);
    }

    /**
     * This method will send the message through existing TCP connection
     * @param message - text to be send
     * @throws IOException
     */
    public void send_message(String message) throws IOException {

        if(out == null) out = new PrintWriter(socket.getOutputStream(), true);
        out.println(message);
        //System.out.println(message);

    }

    /**
     * This will close the TCP connection, and will notify the server about end transmission
     * @throws IOException
     */
    public void close_client_TCP_socket() throws IOException {

        out.println("exit");
        out.close();
        socket.close();
    }
}
