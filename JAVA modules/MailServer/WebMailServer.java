/**
 * Created by eugen on 14/03/15.
 * @author Eugeniu Josan
 * the main class for running the Web Server
 */

import java.io.IOException;


public class WebMailServer {
    public static void main(String[] args) {

        ServerTCPSocket process = new ServerTCPSocket();
        FailedLogs stack = new FailedLogs();

        try {
            process.open_TCP_Socket();
        } catch (IOException e) {
            System.out.println("Mail: Could not listen on port 4444");
            System.exit(-1);
        }

        boolean listening = true;

        while (listening) {
            MultiServerThread thread_work;
            try {
                thread_work = new MultiServerThread(process.getServer().accept(), stack);
                Thread t = new Thread(thread_work);
                t.start();
            }catch (IOException e) {
                System.out.println("Accept failed: 4444");
                System.exit(-1);
            }
        }
    }
}
